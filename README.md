# dubp-sign

Just sign any message with DUBP cryptography.

## Install

    cargo install --git https://git.duniter.org/tools/dubp-sign

## Use

Write the message to be signed in a text file (for example `doc.txt`).

Then run the following command :

    cargo run -- doc.txt

`dubp-sign` will then ask you to enter your credentials, then sign the message with :

    $ cargo run -- doc.txt
        Finished dev [unoptimized + debuginfo] target(s) in 0.04s
        Running `target/release/dubp-sign doc.txt`
    Salt: 
    Password: 
    Your pubkey is8SbT3ZfQA2omAoxh7aKX2CQ1VQwYjaqeixgXRhPP8Ukh
    Signed message:
    my awesome messageg6ULn2fgXjAOUYNz03It6LFX+Wyp1Hj0mrMkuyH7QLoAhGj0Nt8oMYPdOFZUr3eQqyLlDZC/L7ggEm69SdDg==
