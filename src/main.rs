//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use dup_crypto::keys::ed25519::*;
use dup_crypto::keys::{KeyPair as _, Signator as _};
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    /// File to sign
    #[structopt(parse(from_os_str))]
    file: PathBuf,
}

fn main() -> std::io::Result<()> {
    let opt = Opt::from_args();

    let mut file = File::open(opt.file.as_path())?;
    let mut msg = String::new();
    file.read_to_string(&mut msg)?;

    let salt = rpassword::read_password_from_tty(Some("Salt: "))?;
    let password = rpassword::read_password_from_tty(Some("Password: "))?;
    let key_pair = KeyPairFromSaltedPasswordGenerator::with_default_parameters()
        .generate(SaltedPassword::new(salt, password));

    println!("Your pubkey is {}", key_pair.public_key());

    let sig = key_pair.generate_signator().sign(msg.as_bytes());

    println!("Signed message:");
    println!("{}{}", msg, sig);

    Ok(())
}
